import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://integratie2-frontend2019.herokuapp.com/')

WebUI.click(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/button_Log In (2)'))

WebUI.setText(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/input_REGISTER_email (2)'), 'testuser@spagoud.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/input_REGISTER_password (2)'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/button_login (2)'))

WebUI.click(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/a_Hi testUser (1)'))

WebUI.click(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/button_ACCOUNT SETTINGS (1)'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/h1_Welcome testuserspagoudcom'), 
    'Welcome, testuser@spagoud.com')

WebUI.closeBrowser()

