import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://integratie2-frontend2019.herokuapp.com/')

WebUI.click(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/button_Go To Servers (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/input_REGISTER_email (3) (1)'), 'testuser@spagoud.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/input_REGISTER_password (3) (1)'), 
    '1/VWEm4uipk=')

WebUI.click(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/button_login (3) (1)'))

WebUI.click(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/button_Go To Servers (1)'))

WebUI.click(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/i_SPECTATE_fas fa-plus (1)'))

WebUI.setText(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/input_Hi testUser_lobbyName (1)'), 'ATestlobby')

WebUI.click(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/button_Choose Regdar (1)'))

WebUI.click(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/button_create lobby (1)'))

WebUI.navigateToUrl('https://integratie2-frontend2019.herokuapp.com/gameRooms')

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/p_LobbyNameATestlobby (1)'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/p_LobbyOwnertestUser (1)'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/div_ID741LobbyNameATestlobbyLobbyOwnertestUserCapacity14JOINSPECTATE (1)'), 
    0)

WebUI.click(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/button_JOIN'))

WebUI.click(findTestObject('Object Repository/Page_Integratie2FrontEnd2019/button_Remove Lobby'))

WebUI.closeBrowser()

